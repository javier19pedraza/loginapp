package com.prk.parking.controladores;


import com.prk.parking.modelo.UserModel;
import com.prk.parking.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/usuario")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public ArrayList<UserModel> obtenerUsuarios(){
        return userService.obtenerUsuarios();
    }

    @PostMapping()
    public UserModel guardarUser(@RequestBody UserModel user){
        return  this.userService.guardarUser(user);
    }


}
