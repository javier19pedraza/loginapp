package com.prk.parking.services;

import com.prk.parking.modelo.UserModel;
import com.prk.parking.repositorios.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class  UserService {
    @Autowired
    UserRepository userRepository;


    public ArrayList<UserModel> obtenerUsuarios(){
        return (ArrayList<UserModel>) userRepository.findAll();
    }

    public UserModel guardarUser(UserModel user){
        return userRepository.save(user);
    }

}
