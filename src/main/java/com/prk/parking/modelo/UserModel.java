package com.prk.parking.modelo;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "user")
public class UserModel {
    
    //Constructor
    public UserModel(){}

    //Tabla + Getter and Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter @Column(unique =true, nullable = false, name = "id")
    private Long id;
    @Getter @Setter @Column(name = "dni")
    private String dni;
    @Getter @Setter @Column(name = "placa")
    private String placa;
    @Getter @Setter @Column(name = "vehiculo")
    private String vehiculo;
    @Getter @Setter @Column(name = "tiempo")
    private String tiempo;
    

}

